from django.conf.urls import patterns, url

from polls import views

urlpatterns = patterns('',
	#ex: /polls/
	url(r'^$', views.index, name='index'),
	#ex: /polls/34
	url(r'^(?P<question_id>\d+)$', views.detail, name='detail'),
	#ex: /polls/34/results
	url(r'^(?P<question_id>\d+)/results/$', views.results, name='results'),
	#ex:
	url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),

)